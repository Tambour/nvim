"Joel's neovim setup
syntax on

set softtabstop=0


set cc=80
highlight ColorColumn ctermbg=blue

" Tab behaviour
set autoindent
set noexpandtab
set tabstop=4
	set shiftwidth=4

set incsearch

set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:␣
set nowrap
set number
set scrolloff=8

set signcolumn=auto
set noerrorbells


call plug#begin('~/.nvim/plugged')
Plug 'morhetz/gruvbox'
Plug 'https://github.com/vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdtree'
call plug#end()
autocmd vimenter * ++nested colorscheme gruvbox

  if !exists('g:airline_symbols')
    let g:airline_symbols = {}
  endif

  " unicode symbols
  let g:airline_left_sep = '»'
  let g:airline_left_sep = '▶'
  let g:airline_right_sep = '«'
  let g:airline_right_sep = '◀'
  let g:airline_symbols.crypt = '🔒'
  let g:airline_symbols.linenr = '☰'
  let g:airline_symbols.linenr = '␊'
  let g:airline_symbols.linenr = '␤'
  let g:airline_symbols.linenr = '¶'
  let g:airline_symbols.maxlinenr = ''
  let g:airline_symbols.maxlinenr = '㏑'
  let g:airline_symbols.branch = '⎇'
  let g:airline_symbols.paste = 'ρ'
  let g:airline_symbols.paste = 'Þ'
  let g:airline_symbols.paste = '∥'
  let g:airline_symbols.spell = 'Ꞩ'
  let g:airline_symbols.notexists = 'Ɇ'
  let g:airline_symbols.whitespace = 'Ξ'

  " powerline symbols
  let g:airline_left_sep = ''
  let g:airline_left_alt_sep = ''
  let g:airline_right_sep = ''
  let g:airline_right_alt_sep = ''
  let g:airline_symbols.branch = ''
  let g:airline_symbols.readonly = ''
  let g:airline_symbols.linenr = '☰'
  let g:airline_symbols.maxlinenr = ''
  let g:airline_symbols.dirty='⚡'

"Leader
let mapleader = " "

"Show whitespace charecters
noremap <leader>w :set invlist<cr>

"NERDTree
noremap <leader>n :NERDTreeToggle<cr>

"Window Navigation
noremap <leader>h :wincmd h<cr>
noremap <leader>j :wincmd j<cr>
noremap <leader>k :wincmd k<cr>
noremap <leader>l :wincmd l<cr>

"Terminal Navigation
tnoremap <leader>t <C-\><C-n>
